﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PasarYooService.Models
{
    public class Product
    {
        [Key]
        public Guid ProductId { get; set; }

        public Guid StoreId { get; set; }
        public Store Store { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [StringLength(100)]
        public string PriceInfo { get; set; }

        public bool Status { get; set; }

        public bool StockStatus { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int Price { get; set; }
    }
}
