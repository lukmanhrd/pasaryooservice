﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PasarYooService.Models
{
    public class User
    {
        [Key]
        public Guid UserId { get; set; }

        [StringLength(50)]
        [Required]
        public string Username { get; set; }

        [StringLength(1000)]
        public string PasswordSalt { get; set; }

        [StringLength(1000)]
        public string PasswordHash { get; set; }

        [StringLength(250)]
        public string Firstname { get; set; }

        [StringLength(100)]
        [Required]
        public string Lastname { get; set; }

        [StringLength(20)]
        [Required]
        public string PhoneNumber { get; set; }

        [StringLength(150)]
        public string Email { get; set; }

        public bool? BlockedStatus { get; set; }

        [StringLength(1000)]
        public string BlockedReason { get; set; }

        [StringLength(10)]
        public string ForgotPasswordCode { get; set; }

        public DateTime RegistrationDate { get; set; }

        [StringLength(50)]
        [Required]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }
    }
}
