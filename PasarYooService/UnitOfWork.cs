﻿using PasarYooService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PasarYooService.Repositories;

namespace PasarYooService
{
    public class UnitOfWork : IDisposable
    {

        private PasarYooServiceContext dbContext;

        public UserRepository UserRepository { get; }
        public DriverRepository DriverRepository { get; }
        public MarketRepository MarketRepository { get; }
        public ProductRepository ProductRepository { get; }
        public RegionRepository RegionRepository { get; }
        public StoreRepository StoreRepository { get; }
        public TransactionRepository TransactionRepository { get; }
        public TransactionProductRepository TransactionProductRepository { get; }

        public UnitOfWork(PasarYooServiceContext context)
        {
            dbContext = context;

            UserRepository = new UserRepository(context);
            DriverRepository = new DriverRepository(context);
            MarketRepository = new MarketRepository(context);
            ProductRepository = new ProductRepository(context);
            RegionRepository = new RegionRepository(context);
            StoreRepository = new StoreRepository(context);
            TransactionRepository = new TransactionRepository(context);
            TransactionProductRepository = new TransactionProductRepository(context);
        }

        public void Save()
        {
            dbContext.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await dbContext.SaveChangesAsync();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    //dbContext?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UnitOfWork() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
